const quizContainer = document.getElementById('quiz-container');
const submitButton = document.getElementById('submit-btn');

// Define the quiz questions and answers
const questions = [
  {
    question: '1 . What is the capital of AP?',
    answers: [
      'Vizag',
      'Amaravati',
      'Kurnool',
      'None of the Above'
    ],
    correctAnswer: 'none of the Above'
  },
  {
    question: '2 . what type of course are you learning in talentsprints"?',
    answers: [
      'Java',
      'Python',
      'C++',
      'C'
    ],
    correctAnswer: 'Java'
  },
  {
    question: '3 . How many months course duration"?',
    answers: [
      '10',
      '6',
      '4',
      '2'
    ],
    correctAnswer: '4'
  },
  {
    question: '4 . What is 2 + 2?',
    answers: [
      '3',
      '4',
      '5',
      '6'
    ],
    correctAnswer: '4'
  }
  
];

function buildQuiz() {
  const output = [];

  questions.forEach((question, questionIndex) => {
    const answers = [];

    for (const letter in question.answers) {
      answers.push(
        `<label>
          <input type="radio" name="question${questionIndex}" value="${question.answers[letter]}">
          ${question.answers[letter]}
        </label>`
      );
    }

    output.push(
      `<div class="question">
        <h3>${question.question}</h3>
        <div class="answers">${answers.join('')}</div>
      </div>`
    );
  });

  quizContainer.innerHTML = output.join('');
}

function showResults() {
  const answerContainers = quizContainer.querySelectorAll('.answers');
  let numCorrect = 0;

  questions.forEach((question, questionIndex) => {
    const answerContainer = answerContainers[questionIndex];
    const selectedOption = answerContainer.querySelector(`input[name="question${questionIndex}"]:checked`);
    const userAnswer = selectedOption ? selectedOption.value : null;

    if (userAnswer === question.correctAnswer) {
      numCorrect++;
      answerContainer.style.color = 'green';
    } else {
      answerContainer.style.color = 'red';
    }
  });

  alert(`You got ${numCorrect} out of ${questions.length} questions correct!`);
}

buildQuiz();

submitButton.addEventListener('click', showResults);

  
  
