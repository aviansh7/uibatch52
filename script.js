function reverseofnumber(num){
    let reversednum =0;
    while(num!=0){
       reversednum = (reversednum*10)+ num%10;
       num=Math.floor(num/10);
    }
    return reversednum
   
     }
   console.log(reverseofnumber(1234))

   function countOfNums(num){
    let count=0;
    while(num!=0){
    num=Math.floor(num/10);
    count+=1
    }
    return count;
}
console.log(countOfNums(110));


function sumOfDigits(num){
    let sum = 0;

   while(num!=0){
    sum =sum+num%10;
    num=Math.floor(num/10);

   }
   return sum;
}
console.log(sumOfDigits(123));

let word = "hello"
function reverseOfstring(word){
    word.split('').reverse().join('');

}

console.log(word);

function reverseOfstring(str){
    return str;

}
reverseOfstring("hello");

// JavaScript code in order to check string palindrome...
 
let checkPalindrome = (string) => {
    return string === string.split("").reverse("").join("");
}
 
console.log("Is Palindrome : " + checkPalindrome("noon"));
console.log("Is Palindrome : " + checkPalindrome("avinash"));

// perfect number

function perfect(num){

    count=0
    for(i=1;i<=num/2;i++){
        if(num%i==0){
            count=count+i
            
        }
    
    }
    if(num==count){
        return "perfect number"
    }
    else{
        return "not a perfect number"
    }

}
console.log(perfect(6));

function findfactors(num){
    const factors =[];
    let count =0;

    for(let i = 1;i<num/2;i++){
        if(num%i ==0){
            factors.push(i);
            count = i++;

        }
    }

    return 'the factors are ${factors} and count is ${factors.length}'

}

console.log(findfactors(20));

//array methoda

//push and pop
let Numbers = ['one', 'second', 'third'];
Numbers.push('four'); 
console.log(Numbers); 

let removedNumbers= Numbers.pop(); 
console.log(removedNumbers);
console.log(Numbers); 

//shift and unshift

let numbers = [2, 3, 4];
numbers.unshift(1); 
console.log(numbers); 

let removedNumber = numbers.shift(); 
console.log(removedNumber); 
console.log(numbers);

//slice
let animals = ['lion', 'tiger', 'elephant', 'giraffe', 'zebra'];

let slicedAnimals = animals.slice(1, 4); 
console.log(slicedAnimals);

//string methods
//string length

let message = "Hello, world!";
let messageLength = message.length;
console.log(messageLength); 
 
//charAt()
let text = "Hello";

let character = text.charAt(0); 
console.log(character);

//concat()

let firstName = "John";
let lastName = "Doe";

let fullName = firstName.concat(" ", lastName);
console.log(fullName);

//index() and lastindex()

let sentence = "The quick brown fox jumps over the lazy dog";

let indexOfFox = sentence.indexOf("fox");
console.log(indexOfFox);

let lastIndexOfThe = sentence.lastIndexOf("the");
console.log(lastIndexOfThe);

//toupper and tolower
let text2 = "Hello, world!";

let upperCaseText = text2.toUpperCase(); 
console.log(upperCaseText);

let lowerCaseText = text2.toLowerCase(); 
console.log(lowerCaseText);
 
//trim()

let text3 = "   Hello, world!   ";

let trimmedText = text3.trim();
console.log(trimmedText);

//replace

let sentence2 = "The quick brown fox jumps over the lazy dog.";

let newSentence = sentence2.replace("fox", "cat");
console.log(newSentence);

//substring

let text4 = "Hello, world!";

let result1 = text4.substring(0, 5);
console.log(result1);

let result2 = text4.substring(7); 
console.log(result2);

let result3 = text4.substring(-3);
console.log(result3);

let result4 = text4.substring(5, 0); 
console.log(result4);

numbers = [2,4,6,8];
let cube = numbers.map((num)=>num*num*num);
console.log(cube);

numbers = [2,3,4,5,6];
let oddnumbers = numbers.filter((num)=>num%2!=0);
console.log(oddnumbers);
given = ["hello","ap","bye"];
let specified_length = given.filter((given)=>given.length>3);
console.log(specified_length);

let product = numbers.reduce((pro,num)=>pro+num);
console.log(product);