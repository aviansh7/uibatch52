   // Get references to DOM elements
   let listitem = document.getElementById('task');
   let ListElements = document.getElementById('listelements');
   const tasklist = ["css"];

   function addtask() {
       let task = document.getElementById('task').value.trim();
       if (task === "" || task === null) {
           alert("Enter a valid task");
       } else {
           tasklist.push(task);
           displaytask();
           console.log(tasklist)
       }
   }

   function displaytask() {
       // Clear the previous contents of the list
       ListElements.innerHTML = "";


       // Display the updated list of tasks
       tasklist.forEach((item,index) => {
           const li = document.createElement('li');
           li.textContent = item;
          let buttonele = document.createElement('button');
          buttonele.innerHTML = 'delete';
          buttonele.onclick = deleteItem(index);
           
           ListElements.appendChild(li);
           ListElements.appendChild(buttonele);
       });
   }
