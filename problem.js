//find the reverse a num


function reverse_a_number(n)
{
    n = n+"";
    return n.split("").reverse().join("");
}
console.log(Number(reverse_a_number(123)));

//second large num in given array

function nextBiggest(arr) {
    let max = -Infinity, result = -Infinity;
  
    for (const value of arr) {
      const nr = Number(value)
  
      if (nr > max) {
        [result, max] = [max, nr] // save previous max
      } else if (nr < max && nr > result) {
        result = nr; // new second biggest
      }
    }
  
    return result;
  }
  
  const arr = ['20','120','111','215','54','78'];
  console.log(nextBiggest(arr));